import * as compression from "compression";
import * as express from "express";
import * as i18nextMiddleware from "i18next-express-middleware";
import * as Backend from "i18next-node-fs-backend";
import * as next from "next";
import { join } from "path";
import { format, parse } from "url";
import env from "./env";
import { i18nInstance, supportedLanguages } from "./i18n";

export const languageDetector = new i18nextMiddleware.LanguageDetector(null, {
  order: ["enforcedLocale", "languageByDomain"],
});

languageDetector.addDetector({
  name: "enforcedLocale",
  lookup: () => env.ENFORCED_LOCALE,
  cacheUserLanguage: () => {
    /**/
  },
});

languageDetector.addDetector({
  name: "languageByDomain",
  lookup: (opts) => {
    const hostWithoutPort = (opts.headers.host || "").replace(/\:\d+$/, "");
    return hostWithoutPort === env.HOST_RU ? "ru" : "en";
  },
  cacheUserLanguage: () => {
    /**/
  },
});

const app = next({
  dir: __dirname,
  conf: env.NODE_ENV === "production" ? { distDir: "../.next" } : undefined,
  dev: env.NODE_ENV !== "production",
});
const handle = app.getRequestHandler();

const rootStaticFiles = [
  "/browserconfig.xml",
  "/favicon.ico",
  "/manifest.json",
  "/robots.txt",
];

// init i18next with server-side settings
// using i18next-express-middleware
i18nInstance
  .use(Backend)
  .use(languageDetector)
  .init(
    {
      fallbackLng: supportedLanguages[0],
      preload: supportedLanguages,
      ns: ["_error", "common", "index", "photos"], // need to preload all the namespaces
      backend: {
        loadPath: join(__dirname, "../locales/{{lng}}/{{ns}}.json"),
        addPath: join(__dirname, "../locales/{{lng}}/{{ns}}.missing.json"),
      },
    },
    () => {
      app.prepare().then(() => {
        const server = express();

        server.use(compression());

        // enable middleware for i18next
        server.use(i18nextMiddleware.handle(i18nInstance));

        // serve locales for client
        server.use("/locales", express.static(join(__dirname, "../locales")));

        // missing keys
        server.post(
          "/locales/add/:lng/:ns",
          i18nextMiddleware.missingKeyHandler(i18nInstance),
        );

        // redirect legacy CV links
        server.get(
          ["/cv", "/cv_uk", "/cv/uk", "/CVs/Alexander_Kachkaev_CV_UK.pdf"],
          (_, res) => {
            res.redirect("https://www.linkedin.com/in/kachkaev");
          },
        );

        // handle any other requests
        server.get("*", (req, res) => {
          const { pathname, query } = parse(req.url, true);

          if (rootStaticFiles.indexOf(pathname) > -1) {
            const path = join(__dirname, "static", pathname);
            return app.serveStatic(req, res, path);
          }

          if (
            pathname.length > 1 &&
            pathname.slice(-1) === "/" &&
            pathname.indexOf("/_next/") !== 0
          ) {
            return res.redirect(
              format({
                pathname: pathname.slice(0, -1),
                query,
              }),
            );
          }
          (req as any).hostsByLocale = {
            en: env.HOST_EN,
            ru: env.HOST_RU,
          };
          (req as any).gaTrackingId = env.GA_TRACKING_ID;
          (req as any).graphqlUri = env.GRAPHQL_URI;
          (req as any).nodeEnv = env.NODE_ENV;
          return handle(req, res);
        });

        server.listen(env.PORT, (err) => {
          if (err) {
            throw err;
          }
          console.log(`> Ready on port ${env.PORT}`);
        });
      });
    },
  );
