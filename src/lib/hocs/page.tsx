import { TranslationFunction } from "i18next";
import { i18n } from "i18next";
import { compose } from "recompose";
import "../pageEvents";
import withData from "./withData";
import withI18next from "./withI18next";
import withLayout from "./withLayout";

// tslint:disable-next-line:no-var-requires
require("../../styles/index.css");

export default (i18nextNamespaces = ["common"]) =>
  compose<{ t: TranslationFunction; i18n: i18n }, {}>(
    withData,
    withI18next(i18nextNamespaces),
    withLayout,
  );
