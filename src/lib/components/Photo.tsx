import { TranslationFunction } from "i18next";
import { translate } from "react-i18next";
import styled from "styled-components";

const Wrapper = styled.div`
  float: right;
`;
const Img = styled.img`
  display: block;
  border-radius: 5px;
  color: #fff;
`;

export default translate(["index"])(({ t }: { t: TranslationFunction }) => (
  <Wrapper>
    <Img
      width="100"
      height="100"
      alt={t("photoAlt")}
      src="/static/images/alexander_kachkaev.jpg?"
    />
  </Wrapper>
));
