import styled from "styled-components";

const Wrapper = styled.span`
  display: inline-block;
  padding: 0 8px;
`;

const Link = styled<{ notUsed: boolean }, "a">("a")`
  ${(p) => (p.notUsed ? "text-decoration: line-through;" : "")};
`;

export default ({ href, name, notUsed = false }) => (
  <Wrapper>
    <Link notUsed={notUsed} href={href}>
      {name}
    </Link>
  </Wrapper>
);
