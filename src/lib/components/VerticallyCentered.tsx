import styled from "styled-components";

const Wrapper = styled.div`
  display: table-cell;
  width: 100%;
  height: 100%;
  vertical-align: middle;
`;

export default ({ children }) => <Wrapper>{children}</Wrapper>;
