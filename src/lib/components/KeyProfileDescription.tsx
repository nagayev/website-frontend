import { ReactNode } from "react";
import { Trans } from "react-i18next";
import styled from "styled-components";

const KeyProfileDescriptionWrapper = styled.div`
  opacity: 0.4;
  white-space: nowrap;
`;

const KeyProfileDescription = ({
  profilesByName,
  profileName,
  children,
}: {
  profilesByName: any;
  profileName: string;
  children?: ReactNode;
}) => {
  const profile = profilesByName[profileName];
  if (!profile) {
    return <KeyProfileDescriptionWrapper>&nbsp;</KeyProfileDescriptionWrapper>;
  }
  return (
    <KeyProfileDescriptionWrapper>
      <Trans
        i18nKey={`profiles.${profileName}.description`}
        tOptions={profile.info || {}}
      >
        {children}
      </Trans>
    </KeyProfileDescriptionWrapper>
  );
};

export default KeyProfileDescription;
