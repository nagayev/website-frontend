import Head from "next/head";
import Link from "next/link";
import { Trans } from "react-i18next";
import styled from "styled-components";
import Explanation from "../lib/components/Explanation";
import H1 from "../lib/components/H1";
import PhotoSample from "../lib/components/PhotoSample";
import VerticallyCentered from "../lib/components/VerticallyCentered";
import page from "../lib/hocs/page";

const Ul = styled.ul`
  list-style: none;
  padding-left: 20px;
  margin-bottom: 1.5em;

  & li {
    margin-top: 1.5em;
  }
`;

export default page(["photos", "common"])(({ t, i18n }) => (
  <VerticallyCentered>
    <Head>
      <title>{t("title")}</title>
    </Head>
    <H1 error={true}>{t("h1")}</H1>
    <PhotoSample />
    <Explanation>
      <Trans i18nKey="explanation">
        <a href="https://www.flickr.com/photos/kachkaev">flickr</a>
      </Trans>
      <Ul>
        <li>
          <Trans i18nKey="hint1">
            <a
              href={`https://${
                i18n.language
              }.wikipedia.org/wiki/Creative_Commons`}
            >
              cc
            </a>
          </Trans>
        </li>
        <li>
          <Trans i18nKey="hint2" />
        </li>
        <li>
          <Trans i18nKey="hint3">
            <a href="mailto:alexander@kachkaev.ru">email</a>
          </Trans>
        </li>
      </Ul>
      <Link href="/">
        <a>{t("common:signature")}</a>
      </Link>
    </Explanation>
  </VerticallyCentered>
));
